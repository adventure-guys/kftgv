import * as handlers from "./importer.mjs";

/**
 * Configuration of the adventure being exported/imported
 */
export const ADVENTURE = {

  // Basic information about the module and the adventure compendium it contains
  moduleName: "ag-kftgv",
  packName: "kftgv",
  packId: "ag-kftgv.kftgv",
  adventureUuid: "Compendium.ag-kftgv.kftgv.Adventure.YIs3pmARLMem8TDB",
  adventureId: "YIs3pmARLMem8TDB",

  // A CSS class to automatically apply to application windows which belong to this module
  cssClass: "ag-kftgv",

  description: `В сборнике «Ключи от Золотого хранилища» представлено тринадцать приключений Dungeons & Dragons, в которых есть ограбления. В каждом из них персонажи получают миссию, планируют дело, реализуют свой план и пытаются сбежать с места происшествия.`,

  // Define special Import Options with custom callback logic
  importOptions: {
    displayJournal: {
      label: "Открыть руководство по модулю",
      default: true,
      handler: handlers.displayJournal,
      documentId: "oxJ41Sf6pZPRKCsL"
    },
    customizeJoin: {
      label: "Преобразовать игровой мир",
      default: false,
      handler: handlers.customizeJoin,
      background: "modules/ag-kftgv/assets/art/adventure-cover.webp"
    }
  },

  // The ID of the 'Getting Started' journal to determine if the adventure has been imported before.
  gettingStartedId: "oxJ41Sf6pZPRKCsL"
};
